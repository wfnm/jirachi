module wooloo.farm/jirachi

go 1.13

require (
	github.com/bwmarrin/discordgo v0.22.0
	github.com/gorilla/csrf v1.6.2 // indirect
	github.com/gorilla/handlers v1.4.2 // indirect
	github.com/gorilla/mux v1.7.3 // indirect
	github.com/gorilla/sessions v1.2.0 // indirect
	github.com/hashicorp/golang-lru v0.5.4
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/lib/pq v1.3.0
	github.com/valyala/quicktemplate v1.4.1 // indirect
	golang.org/x/net v0.0.0-20191209160850-c0dbc17a3553 // indirect
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d // indirect
)
