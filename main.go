package main

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"os"
	"os/signal"
	"strings"
	"sync"
	"syscall"
	"text/template"

	"github.com/bwmarrin/discordgo"
	"github.com/kelseyhightower/envconfig"
	"github.com/lib/pq"
	_ "github.com/lib/pq"
)

type Config struct {
	Token              string   `default:""`
	Dsn                string   `default:""`
	StarboardChannelID string   `default:""`
	Admonition         string   `default:"Sorry, <@{{.UserID}}>, but you can't star your own message!"`
	Emojis             []string `default:"⭐"`
	MinimumStars       int      `default:"3"`
	MessageCacheSize   int      `default:"1000"`
}

type Bot struct {
	session   *discordgo.Session
	emojisSet map[string]bool
	db        *sql.DB
	config    *Config

	userID     string
	admonition *template.Template

	messageCacheMu sync.RWMutex
	messageCache   *ARCCache

	channelGuildIDsMu sync.RWMutex
	channelGuildIDs   map[string]string
}

func newBot(c *Config) (*Bot, error) {
	session, err := discordgo.New(c.Token)
	if err != nil {
		return nil, err
	}

	session.StateEnabled = false
	session.SyncEvents = true

	c.Token = ""
	log.Printf("Config: %+v\n", c)

	emojisSet := make(map[string]bool, len(c.Emojis))
	for _, emoji := range c.Emojis {
		emojisSet[emoji] = true
	}

	db, err := sql.Open("postgres", c.Dsn)

	messageCache, err := NewARC(c.MessageCacheSize)
	if err != nil {
		return nil, err
	}

	var admonition *template.Template
	if c.Admonition != "" {
		var err error
		admonition, err = template.New("").Parse(c.Admonition)
		if err != nil {
			return nil, err
		}
	}

	b := &Bot{
		session:      session,
		emojisSet:    emojisSet,
		db:           db,
		messageCache: messageCache,
		config:       c,

		admonition:      admonition,
		channelGuildIDs: make(map[string]string),
	}

	session.Identify.Intents = discordgo.MakeIntent(
		discordgo.IntentsGuilds |
			discordgo.IntentsGuildMessages |
			discordgo.IntentsGuildMessageReactions)
	session.AddHandler(b.ready)
	session.AddHandler(b.guildCreate)
	session.AddHandler(b.channelCreate)
	session.AddHandler(b.channelDelete)
	session.AddHandler(b.messageReactionAdd)
	session.AddHandler(b.messageReactionRemove)
	session.AddHandler(b.messageCreate)
	session.AddHandler(b.messageUpdate)
	session.AddHandler(b.messageDelete)
	session.AddHandler(b.messageDeleteBulk)

	return b, nil
}

func (b *Bot) ready(_ *discordgo.Session, event *discordgo.Ready) {
	b.userID = event.User.ID
	go b.session.UpdateStatus(0, "mmm yummy stars")
}

func (b *Bot) channelCreate(_ *discordgo.Session, event *discordgo.ChannelCreate) {
	b.channelGuildIDsMu.Lock()
	defer b.channelGuildIDsMu.Unlock()
	b.channelGuildIDs[event.ID] = event.GuildID
}

func (b *Bot) channelDelete(_ *discordgo.Session, event *discordgo.ChannelDelete) {
	b.channelGuildIDsMu.Lock()
	defer b.channelGuildIDsMu.Unlock()
	delete(b.channelGuildIDs, event.ID)
}

func (b *Bot) messageToStarboardEmbed(msg *discordgo.Message, starCount int) *discordgo.MessageEmbed {
	embed := &discordgo.MessageEmbed{
		Color: 0xffe65a,
		Thumbnail: &discordgo.MessageEmbedThumbnail{
			URL: msg.Author.AvatarURL(""),
		},
		Timestamp: string(msg.Timestamp),
		Fields: []*discordgo.MessageEmbedField{
			&discordgo.MessageEmbedField{
				Name:   "Author",
				Value:  "<@" + msg.Author.ID + ">",
				Inline: true,
			},
			&discordgo.MessageEmbedField{
				Name:   "Channel",
				Value:  "<#" + msg.ChannelID + ">",
				Inline: true,
			},
		},
		Footer: &discordgo.MessageEmbedFooter{
			Text: fmt.Sprintf("⭐ %d", starCount),
		},
	}

	if msg.Content != "" {
		embed.Fields = append(embed.Fields, &discordgo.MessageEmbedField{
			Name:  "Message",
			Value: msg.Content,
		})
	}

	guildID := func() string {
		b.channelGuildIDsMu.RLock()
		defer b.channelGuildIDsMu.RUnlock()
		return b.channelGuildIDs[msg.ChannelID]
	}()
	embed.Fields = append(embed.Fields, &discordgo.MessageEmbedField{
		Name:  "Context",
		Value: fmt.Sprintf("[Jump](https://discordapp.com/channels/%s/%s/%s)", guildID, msg.ChannelID, msg.ID),
	})

	if len(msg.Attachments) > 0 {
		embed.Image = &discordgo.MessageEmbedImage{
			URL: msg.Attachments[0].URL,
		}
	}

	return embed
}

func (b *Bot) guildCreate(_ *discordgo.Session, event *discordgo.GuildCreate) {
	for _, channel := range event.Channels {
		func() {
			b.channelGuildIDsMu.Lock()
			defer b.channelGuildIDsMu.Unlock()
			b.channelGuildIDs[channel.ID] = event.Guild.ID
		}()
	}
}

func (b *Bot) messageCreate(_ *discordgo.Session, event *discordgo.MessageCreate) {
	b.messageCacheMu.Lock()
	defer b.messageCacheMu.Unlock()

	b.messageCache.Add(event.ID, event.Message)
}

func (b *Bot) messageUpdate(_ *discordgo.Session, event *discordgo.MessageUpdate) {
	b.messageCacheMu.Lock()
	defer b.messageCacheMu.Unlock()

	cachedMsg, ok := b.messageCache.Get(event.ID)
	if !ok {
		// Don't the updated message.
		return
	}

	msg := cachedMsg.(*discordgo.Message)
	if event.Message.Author == nil {
		// The update sometimes contains no author, so let's include it.
		event.Message.Author = msg.Author

		// I don't know what the deal with reactions are, just use the ones we know.
		event.Message.Reactions = msg.Reactions
	}

	b.messageCache.Add(event.ID, event.Message)
}

func (b *Bot) messageDelete(_ *discordgo.Session, event *discordgo.MessageDelete) {
	ctx := context.Background()

	if event.ChannelID != b.config.StarboardChannelID {
		return
	}

	if _, err := b.db.ExecContext(ctx, `
		delete from starboard
		where starboard_message_id = $1
	`, event.ID); err != nil {
		log.Printf("Failed to delete starboard message: %s", err)
		return
	}
}

func (b *Bot) messageDeleteBulk(_ *discordgo.Session, event *discordgo.MessageDeleteBulk) {
	ctx := context.Background()

	if event.ChannelID != b.config.StarboardChannelID {
		return
	}

	if _, err := b.db.ExecContext(ctx, `
		delete from starboard
		where starboard_message_id = any($1)
	`, pq.Array(event.Messages)); err != nil {
		log.Printf("Failed to delete starboard message: %s", err)
		return
	}
}

func (b *Bot) getMessage(channelID string, messageID string) (*discordgo.Message, error) {
	// If the message is in cache, return it.
	msg := func() *discordgo.Message {
		b.messageCacheMu.RLock()
		defer b.messageCacheMu.RUnlock()

		msg, ok := b.messageCache.Get(messageID)
		if !ok {
			return nil
		}

		return msg.(*discordgo.Message)
	}()

	if msg != nil {
		return msg, nil
	}

	// Try fetch the message.
	msg, err := b.session.ChannelMessage(channelID, messageID)
	if err != nil {
		return nil, err
	}

	// Try to put the message into the cache, with caveats:
	//
	// - If it already exists, return it and ignore the message we fetched.
	// - Otherwise, update the cache and return.
	b.messageCacheMu.Lock()
	defer b.messageCacheMu.Unlock()

	curMsg, ok := b.messageCache.Get(messageID)
	if ok {
		return curMsg.(*discordgo.Message), nil
	}

	b.messageCache.Add(messageID, msg)
	return msg, nil
}

func (b *Bot) messageReactionRemove(_ *discordgo.Session, event *discordgo.MessageReactionRemove) {
	b.messageCacheMu.Lock()
	defer b.messageCacheMu.Unlock()

	rawMsg, ok := b.messageCache.Get(event.MessageID)
	if !ok {
		return
	}

	msg := *rawMsg.(*discordgo.Message)
	{
		reactions := make([]*discordgo.MessageReactions, len(msg.Reactions))
		copy(reactions, msg.Reactions)
		msg.Reactions = reactions
	}
	for i, reaction := range msg.Reactions {
		if reaction.Emoji.ID == event.Emoji.ID && reaction.Emoji.Name == event.Emoji.Name {
			reaction.Count--
			if reaction.Count <= 0 {
				msg.Reactions = append(msg.Reactions[:i], msg.Reactions[i+1:]...)
			}
			break
		}
	}

	b.messageCache.Add(msg.ID, &msg)
}

func (b *Bot) messageReactionAdd(_ *discordgo.Session, event *discordgo.MessageReactionAdd) {
	// Update the message state in the cache.
	func() {
		b.messageCacheMu.Lock()
		defer b.messageCacheMu.Unlock()

		rawMsg, ok := b.messageCache.Get(event.MessageID)
		if !ok {
			return
		}

		msg := *rawMsg.(*discordgo.Message)
		{
			reactions := make([]*discordgo.MessageReactions, len(msg.Reactions))
			copy(reactions, msg.Reactions)
			msg.Reactions = reactions
		}

		var hasReaction bool
		for _, reaction := range msg.Reactions {
			if reaction.Emoji.ID == event.Emoji.ID && reaction.Emoji.Name == event.Emoji.Name {
				reaction.Count++
				hasReaction = true
				break
			}
		}
		if !hasReaction {
			msg.Reactions = append(msg.Reactions, &discordgo.MessageReactions{
				Count: 1,
				Me:    event.UserID == b.userID,
				Emoji: &event.Emoji,
			})
		}

		b.messageCache.Add(msg.ID, &msg)
	}()

	ctx := context.Background()

	if event.ChannelID == b.config.StarboardChannelID {
		return
	}

	emoji := event.Emoji.ID
	if emoji == "" {
		emoji = event.Emoji.Name
	}
	if !b.emojisSet[emoji] {
		return
	}

	var ok bool
	defer func() {
		if !ok {
			if err := b.session.MessageReactionRemove(event.ChannelID, event.MessageID, emoji, event.UserID); err != nil {
				log.Printf("Failed to remove emoji: %s", err)
				return
			}
		}
	}()

	msg, err := b.getMessage(event.ChannelID, event.MessageID)
	if err != nil {
		log.Printf("Failed to get message: %s", err)
		return
	}

	if msg.Author.ID == event.UserID {
		if b.admonition != nil {
			var buf strings.Builder
			if err := b.admonition.Execute(&buf, struct{ UserID string }{event.UserID}); err != nil {
				log.Printf("Failed to admonish: %s", err)
				return
			}

			if _, err := b.session.ChannelMessageSend(event.ChannelID, buf.String()); err != nil {
				log.Printf("Failed to admonish: %s", err)
			}
		}
		return
	}

	var starCount int
	for _, reaction := range msg.Reactions {
		emoji := reaction.Emoji.ID
		if emoji == "" {
			emoji = reaction.Emoji.Name
		}

		if b.emojisSet[emoji] {
			starCount += reaction.Count
		}
	}

	if starCount < b.config.MinimumStars {
		ok = true
		return
	}

	tx, err := b.db.BeginTx(ctx, &sql.TxOptions{})
	if err != nil {
		log.Printf("Failed to start transaction: %s", err)
		return
	}
	defer tx.Rollback()

	var starboardMessageID string
	if err := tx.QueryRowContext(ctx, `
		select starboard_message_id from starboard
		where message_id = $1
	`, &event.MessageID).Scan(&starboardMessageID); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			starboardMsg, err := b.session.ChannelMessageSendComplex(b.config.StarboardChannelID, &discordgo.MessageSend{
				Embed: b.messageToStarboardEmbed(msg, starCount),
			})
			if err != nil {
				log.Printf("Failed to send message to starboard: %s", err)
				return
			}
			starboardMessageID = starboardMsg.ID

			if _, err := tx.ExecContext(ctx, `
				insert into starboard (message_id, starboard_message_id)
				values ($1, $2)
			`, msg.ID, starboardMessageID); err != nil {
				log.Printf("Failed to save starboard message: %s", err)
				return
			}

			ok = true
		} else {
			log.Printf("Failed to get row: %s", err)
			return
		}
	} else {
		if _, err := b.session.ChannelMessageEditComplex(&discordgo.MessageEdit{
			Channel: b.config.StarboardChannelID,
			ID:      starboardMessageID,
			Embed:   b.messageToStarboardEmbed(msg, starCount),
		}); err != nil {
			log.Printf("Failed to update message: %s", err)
			return
		}
		ok = true
	}

	if err := tx.Commit(); err != nil {
		log.Printf("Failed to commit transaction: %s", err)
	}
}

func (b *Bot) run() error {
	if err := b.session.Open(); err != nil {
		return err
	}

	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	// Cleanly close down the Discord session.
	b.session.Close()
	return nil
}

func main() {
	var c Config
	if err := envconfig.Process("jirachi", &c); err != nil {
		log.Fatalf("Failed to parse config: %s", err)
	}

	bot, err := newBot(&c)
	if err != nil {
		log.Fatalf("failed to create discord client: %s", err)
	}

	if err := bot.run(); err != nil {
		log.Fatalf("failed to connect to discord: %s", err)
	}
}
